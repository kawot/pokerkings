﻿Shader "Unlit/CardShade"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Center("Center", Float) = 0.5
		_CenterWidth("Width", Float) = 0.01
		_ShadeDistance("Distance", Float) = 0.1
		_Intensity("Intensity", Range(0.0, 1.0)) = 1.0
    }
    SubShader
    {
		Blend SrcAlpha OneMinusSrcAlpha

        Tags 
		{
			"Queue" = "Transparent"
			"RenderType"="Transparent" 
		}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 screenPos : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.screenPos = ComputeScreenPos(o.vertex);
                return o;
            }

			float _Center;
			float _CenterWidth;
			float _ShadeDistance;
			fixed _Intensity;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
				float h = i.screenPos.y;
				float shadeDist = _ShadeDistance * 19.0 * _ScreenParams.x / (9.0 * _ScreenParams.y);
				fixed shade = saturate(abs(h - _Center) - _CenterWidth) * 100.0 / (shadeDist);
				shade = (1.0 - shade * shade);
				col.rgb = lerp(col.rgb, col.rgb*shade, _Intensity);
                return col;
            }
            ENDCG
        }
    }
}
