﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using Random = UnityEngine.Random;

public class Dealer : MonoBehaviour
{
    public string[] cards = new string[52] {   "♠2", "♠3","♠4","♠5","♠6","♠7","♠8","♠9","♠T","♠J","♠Q","♠K","♠A",
                                        "♣2", "♣3","♣4","♣5","♣6","♣7","♣8","♣9","♣T","♣J","♣Q","♣K","♣A",
                                        "♦2", "♦3","♦4","♦5","♦6","♦7","♦8","♦9","♦T","♦J","♦Q","♦K","♦A",
                                        "♥2", "♥3","♥4","♥5","♥6","♥7","♥8","♥9","♥T","♥J","♥Q","♥K","♥A",
                                       };
    private Dictionary<string, float> rnd;
    private List<Card> cardList = new List<Card>();
    //private string foundedPairs;
    //private string foundedTriplets;
    //private string foundedStraight;
    //private string foundedFlush;
    //private Card foundedHiCard;
    private Game game;
    private string foundedRoyal;
    private string comba;
    private List<Card> bestList;
    public string[] combaNames = {"High Card", "Pair", "Two Pair", "Three of a Kind", "Straight", "Flush", "Full House", "Four of a Kind", "Straight Flush", "Royal Flush"};
    public int coinIndex = -1;
    private float bestHandValue = -1;
    //private string foundedFour;
    //private string foundedStraightFlush;
    //private string foundedFullHouse;
    public Image fasad;
    private TextMeshProUGUI currentWinLabel;
    private Text combaNameLabel;
    public Sprite fasadsprite;
    public Sprite pressed;
    //private string Dealerout;

    public bool isSpinning = false;

    public void Start()
    {
        currentWinLabel = GameObject.Find("CurrentWin").GetComponent<TextMeshProUGUI>();
        combaNameLabel = GameObject.Find("CombaName").GetComponent<Text>();
        fasadsprite = fasad.sprite;
        game = GetComponent<Game>();
        string dump = "";int i = 0;
        foreach(string caca in cards)
            dump += caca+"\t"+(i++).ToString("00") + "\n";

        SpinController.instance.ResetSlot(GetRandomCards(15));


        //var testList = Card.ParceList("♣2, ♠4, ♣A, ♣3, ♠5");
        //Debug.Log("Test List: " + CardList(testList));
        //Debug.Log(HandChecker.Straight(testList));
    }
    public void Pressed()
    {
        fasad.sprite = pressed;

        if (isSpinning) return;

        isSpinning = true;
        SoundBank.Play("rolling");

        Deal();
    }
    public void Released()
    {
        fasad.sprite = fasadsprite;
    }

    public List<Card> AppendList(List<Card> list)
    {
        List<Card> lc = list;
        while (list.Count < 5)
        {
            Card card = new Card(cards[Random.Range(0, 51)]);
            
            if (String.Join("",list.ToArray()).IndexOf(card.ToString())==-1&& String.Join("", list.ToArray()).IndexOf(card.value.ToString()) == -1)//1 && String.Join("", list.ToArray()).IndexOf(card.suit) == -1)
            {
                lc.Add(card);
            }
        }

        return lc;
    }

    private string Multyply(string ret, char t, int v)
    {
        string s = ret;
        for (int i = 0; i < v; i++)
            s += t;
        return s;
    }
    public List<Card> secondRow;
    public List<Card> firstRow;

    public List<Card> GetRandomCards(int count)
    {
        rnd = new Dictionary<string, float>();
        var result = new List<Card>();
        foreach (string card in cards)
        {
            float r = Random.Range(0f, 1f);
            rnd.Add(card, r);
            Card c = new Card(card);

        }


        for (int k = 0; k < count; k++)


        {
            int maxindex = 0;

            for (int i = 0; i < rnd.Count; i++)
            {
                if (rnd.ElementAt(i).Value > rnd.ElementAt(maxindex).Value)
                    maxindex = i;
            }

            result.Add(new Card(rnd.ElementAt(maxindex).Key));
            rnd.Remove(rnd.ElementAt(maxindex).Key);
        }

       

        //PrintCardList(count + " random cards generated: ", result);

        return result;
    }

    public void Deal()
    {
        //Game.OrderList = game.config.orderCards;

        currentWinLabel.text = "";
        combaNameLabel.text = "";

        cardList = GetRandomCards(20);
        //cardList = Tools.ParceList("♠7, ♠9, ♠A, ♥J, ♣2, ♠J, ♠6, ♠3, ♠2, ♦K, ♠T, ♥6, ♠K, ♥4, ♥A, ♦2, ♠Q, ♣6, ♣K, ♣7");
        Debug.Log("20 random cards: " + CardList(cardList));

        firstRow = cardList.GetRange(0, 5);
        secondRow = cardList.GetRange(5, 5);
        var topRow = cardList.GetRange(10, 5);
        var bottomRow = cardList.GetRange(15, 5);
        Debug.Log("*** first set: " + CardList(firstRow));
        Debug.Log("*** second set: " + CardList(secondRow));
        Debug.Log("--- top row: " + CardList(topRow));
        Debug.Log("--- bottom row: " + CardList(bottomRow));
        
        coinIndex = -1;
        bestHandValue = -1f;
        int comnum;
        for (comnum = 0; comnum < 32; comnum++)
        {
            string coms = Convert.ToString(comnum, 2).PadLeft(5, '0');

            var checkList = new List<Card>();
            for (int i = 0; i < 5; i++)
            {
                if (coms[i] == '0')
                    checkList.Add(firstRow.ElementAt(i));
                else
                    checkList.Add(secondRow.ElementAt(i));
            }
            var sortedList = Game.Sorted(checkList);
            var tieBreaker = 0f;

            var straight = HandChecker.Straight(sortedList);
            var flush = HandChecker.Flush(sortedList);

            //Royal Flush
            if (straight > 0 && flush > 0 && sortedList[4].valueint == 14 && sortedList[0].valueint == 10)
            {
                CheckIfHandBetter(9, checkList);
                continue;
            }
            //Straight Flush
            if (straight > 0 && flush > 0)
            {
                CheckIfHandBetter(8 + straight, checkList);
                continue;
            }
            //Four of a Kind
            tieBreaker = HandChecker.Four(sortedList);
            if (tieBreaker > 0)
            {
                CheckIfHandBetter(7 + tieBreaker, checkList);
                continue;
            }
            //Full House
            tieBreaker = HandChecker.FullHouse(sortedList);
            if (tieBreaker > 0)
            {
                CheckIfHandBetter(6 + tieBreaker, checkList);
                continue;
            }
            //Flush
            if (flush > 0)
            {
                CheckIfHandBetter(5 + flush, checkList);
                continue;
            }
            //Straight
            if (straight > 0)
            {
                CheckIfHandBetter(4 + straight, checkList);
                continue;
            }
            //Three of a Kind
            tieBreaker = HandChecker.Three(sortedList);
            if (tieBreaker > 0)
            {
                CheckIfHandBetter(3 + tieBreaker, checkList);
                continue;
            }
            //Two Pair
            tieBreaker = HandChecker.TwoPair(sortedList);
            if (tieBreaker > 0)
            {
                CheckIfHandBetter(2 + tieBreaker, checkList);
                continue;
            }
            //Pair
            tieBreaker = HandChecker.Pair(sortedList);
            if (tieBreaker > 0)
            {
                CheckIfHandBetter(1 + tieBreaker, checkList);
                continue;
            }
            //HighCard
            CheckIfHandBetter(HandChecker.HighCard(sortedList), checkList);
        }
        coinIndex = Mathf.FloorToInt(bestHandValue);

        Debug.Log(" ");
        Debug.Log("Best combination is " + CardList(bestList) + " with best hand: " + combaNames[coinIndex]);
        
        SpinController.instance.Spin(bestList, topRow, bottomRow);
        
        game.SaveCombination(CardList(bestList), CardList(Game.Sorted(bestList)), combaNames[coinIndex]);
    }

    private void CheckIfHandBetter(float handValue, List<Card> checkList)
    {
        //Debug.Log("best hand for " + CardList(checkList) + " with tb " + (handValue % 1f) + " is " + combaNames[Mathf.FloorToInt(handValue)]);
        if (handValue > bestHandValue)
        {
            bestHandValue = handValue;
            bestList = checkList;
        }
    }

    private void PrintCardList(string v, List<Card> cardslist, bool b=false)
    {
        string s = v;
        foreach (Card c in cardslist)
        {
            s += c.suit.ToString() + c.value.ToString() + ", ";
            if (cardslist.IndexOf(c)+1 == cardslist.Count / 2 && b)
                s += "  //   ";
                }
        print(s);
    }

    private string CardList(List<Card> cardList)
    {
        return Tools.CardList(cardList);
    }
}

public struct Card
{

    public char suit;
    public int suitint;
    public char value;
    public int valueint
    {
        get
        {
            return ("0123456789TJQKA").IndexOf(this.value);
        }
        set
        {
            this.value = ("01235456789TJQKA")[value];
        }
    }
    public Card(string card)
    {
        //Debug.Log("card=" + card);
        this.suit = card[0];
        this.value = card[1];
        this.suitint = ("♠♣♦♥").IndexOf(this.suit);
        if (this.suitint == -1)
            Debug.Log("WWWWWWWWWWWWWWWWWWWWWWWWWWWW--" + card[0]);

    }
    public override string ToString()
    {
        return "" + suit.ToString() + value.ToString();
    }


}

