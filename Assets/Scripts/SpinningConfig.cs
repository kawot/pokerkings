﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[CreateAssetMenu(fileName = "SpinningConfig", menuName = "Settings/SpinningConfig", order = 1)]
public class SpinningConfig : ScriptableObject
{
    [Tooltip("Spinning speed")]
    public float spinningSpeed = 5f;
    [Tooltip("Full rotations (3 cards change) before stop. For each reel")]
    public int[] fullRotations = { 3, 4, 5, 6, 7 };
    [Tooltip("Overspin (slingshot) max amount")]
    [Range(0f, 1f)]
    public float overSpin = 0.1f;
    [Tooltip("Overspin (slingshot) randomisation (0 - allways max, 1 - random from 0 to max)")]
    [Range(0f, 1f)]
    public float overSpinRnd = 0.2f;
    [Tooltip("Back spin (return after overspin) duration")]
    public float backSpinTime = 0.2f;
    [Tooltip("Back spin (return after overspin) ease")]
    public Ease backSpinEase = Ease.OutBack;
    [Range(0f, 1f)]
    public float backSpinOvershot = 0.5f;
    public Ease overshotEase = Ease.InQuad;
}
