﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Tools
{
    public static string CardList(List<Card> cardList)
    {
        var s = "";
        for (int i = 0; i < cardList.Count; i++)
        {
            s += cardList[i].suit.ToString() + cardList[i].value.ToString();
            if (i != cardList.Count - 1) s += ", ";
        }
        return s;
    }

    internal static List<Card> ParceList(string straight)
    {
        string[] cardstring = straight.Split(',');
        List<Card> ret = new List<Card>();
        foreach (string c in cardstring)
        {
            string card = c.Trim();
            if (card != "")
                ret.Add(new Card(card));
        }
        return ret;
    }
}
