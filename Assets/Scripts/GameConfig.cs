﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "GameConfig", menuName = "Settings/GameConfig", order = 1)]
public class GameConfig : ScriptableObject
{
    [Tooltip("Sort final card set")]
    public bool orderCards = false;
}

