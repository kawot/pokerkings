﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Random = UnityEngine.Random;

public class SpinController : MonoBehaviour
{
    public static SpinController instance;

    [SerializeField] private Game game;
    [SerializeField] private Transform[] reels;
    [SerializeField] private SpinningConfig config;
    [Space()]
    [Header("Board")]
    public Transform board;
    public Transform size;
    public Transform threshold;
    public Transform tile;
    private int[] rotations;
    private float accelerateDuration;
    private float decelerateDuration;

    private Image[] images;
    private List<Card> currentCards;
    private ReelState[] reelStates;
    private float[] phaseTimer;
    private int[] cardsShown;
    private Card[,] cardBoard;

    public enum ReelState
    {
        Still,
        Spinning,
        Stoping
    }

    void Awake()
    {
        instance = this;
        images = board.GetComponentsInChildren<Image>();
        reelStates = new ReelState[reels.Length];
        rotations = new int[reels.Length];
        phaseTimer = new float[reels.Length];
        cardsShown = new int[reels.Length];
        cardBoard = new Card[reels.Length, 3];
    }

    void Update()
    {
        for (var i = 0; i < reels.Length; i++)
        {
            if (reelStates[i] != ReelState.Spinning) continue;

            var reelThreshold = (threshold.position.y - board.position.y) * 2f;
            rotations[i] = Mathf.CeilToInt((board.position.y - reels[i].position.y) / reelThreshold);

            var speed = config.spinningSpeed;
            if (rotations[i] <= 1)
            {
                phaseTimer[i] -= Time.deltaTime;
                var arg = 1f - phaseTimer[i] / accelerateDuration;
                speed *= arg;
            }
            else if (rotations[i] < config.fullRotations[i])
            {
                phaseTimer[i] = decelerateDuration;
            }
            else
            {
                phaseTimer[i] -= Time.deltaTime;
                var arg = phaseTimer[i] / decelerateDuration;
                speed *= arg;
                if (phaseTimer[i] < 0f)
                {
                    BackSpin(i);
                    continue;
                }
            }
            reels[i].position += Vector3.down * speed * (size.position.y - board.position.y) * Time.deltaTime;
        }
    }

    public void Spin(List<Card> finalCards, List<Card> topRow, List<Card> bottomRow)
    {
        if (Game.OrderList) finalCards = Game.Sorted(finalCards);
        for (var i = 0; i < reels.Length; i++)
        {
            reelStates[i] = ReelState.Spinning;

            var fullRotationDistance = (threshold.position.y - board.position.y) * 2f;
            var maxSpeed = config.spinningSpeed * (size.position.y - board.position.y);
            accelerateDuration = 2f * fullRotationDistance / maxSpeed;
            //var overSpinDistance = fullRotationDistance * (1f + config.overSpin / 3f);
            var overspin = Mathf.Lerp(1f, Random.Range(0f, 1f), config.overSpinRnd) * config.overSpin;
            var overSpinDistance = fullRotationDistance * (1f + overspin / 3f);
            decelerateDuration = 2f * overSpinDistance / maxSpeed;

            phaseTimer[i] = accelerateDuration;
            cardsShown[i] = 0;

            cardBoard[i, 0] = bottomRow[i];
            cardBoard[i, 1] = finalCards[i];
            cardBoard[i, 2] = topRow[i];
        }
    }

    public void ResetSlot(List<Card> cards)
    {
        if (images.Length > cards.Count) Debug.LogError("not enough cards (" + cards.Count + ") for all card images (" + images.Length);
        for (var i = 0; i < images.Length; i++)
        {
            images[i].sprite = game.CardSprite(cards[i]);
        }
    }

    public void SetCenterRow(List<Card> cards)
    {
        for (var i = 0; i < cards.Count; i++)
        {
            images[i * 3 + 1].sprite = game.CardSprite(cards[i]);
        }
    }

    public Sprite SpriteForReel(int reel, ref bool blurred)
    {
        if (rotations[reel] < config.fullRotations[reel])
        {
            blurred = true;
            return game.RandomCardSprite();
        }
        else
        {
            blurred = false;
            var sprite = game.CardSprite(cardBoard[reel, cardsShown[reel]]);
            cardsShown[reel]++;
            if (cardsShown[reel] > 2) cardsShown[reel] = cardsShown[reel] % 3;
            return sprite;
        }
    }

    private void BackSpin(int i)
    {
        SoundBank.PlayRandom("stop", 3);
        reelStates[i] = ReelState.Stoping;
        reels[i].localPosition = new Vector3(reels[i].localPosition.x, reels[i].localPosition.y % ((threshold.position.y - board.position.y) * 2f), 0f);
        //reels[i].DOLocalMoveY(0f, config.backSpinTime).SetEase(config.backSpinEase).OnComplete(() => OnReelStop(i));       
        var backspinSequence = DOTween.Sequence();
        backspinSequence.
            Append(reels[i].DOLocalMoveY(-reels[i].localPosition.y*config.backSpinOvershot,config.backSpinTime).SetEase(config.backSpinEase)).
            Append(reels[i].DOLocalMoveY(0f,config.backSpinTime*config.backSpinOvershot).SetEase(config.overshotEase)).
            OnComplete(() => OnReelStop(i));
    }

    private void OnReelStop(int index)
    {      
        reelStates[index] = ReelState.Still;       
        CheckIfAllStoped();
    }

    private void CheckIfAllStoped()
    {
        for (int i = 0; i < reels.Length; i++)
        {
            if (reelStates[i] != ReelState.Still) return;
        }
        game.Show();
    }
}
