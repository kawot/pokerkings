﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "BendingConfig", menuName = "Settings/BendingConfig", order = 1)]
public class BendingConfig : ScriptableObject
{
    //public Vector2 slotCenter;
    //public Vector2 slotSize;
    //[Space()]
    [Header("Bending Parameters")]
    public AnimationCurve scaleFromY;
    public AnimationCurve pitchFromY;
    public float pitchMultiplier;
    [Space()]
    public AnimationCurve factorFromX;
    public AnimationCurve offsetFromY;
    public float offsetMultiplier;
    public AnimationCurve rollFromY;
    public float rollMultiplier;
    [Space()]
    public float blurredScaleFactor = 1.2f;
}
