﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class HandChecker
{
    public static float Straight(List<Card> sorted)
    {
        if (sorted[0].valueint == 2 && 
            sorted[1].valueint == 3 && 
            sorted[2].valueint == 4 && 
            sorted[3].valueint == 5 && 
            sorted[4].valueint == 14) return 0.05f;
        for (int i = 0; i < 4; i++)
        {
            if (sorted[i].valueint + 1 != sorted[i + 1].valueint) return 0;
        }
        return TieBreaker(sorted[4].valueint);
    }

    public static float Flush(List<Card> sorted)
    {
        var suit = sorted[0].suit;
        for (int i = 1; i < 5; i++)
        {
            if (sorted[i].suit != suit) return 0;
        }
        return TieBreaker(sorted[4].valueint, sorted[3].valueint, sorted[2].valueint, sorted[1].valueint, sorted[0].valueint);
    }

    public static float HighCard(List<Card> sorted)
    {
        return TieBreaker(sorted[4].valueint, sorted[3].valueint, sorted[2].valueint, sorted[1].valueint, sorted[0].valueint);
    }

    public static float Four(List<Card> sorted)
    {
        int t1 = sorted[2].valueint;

        if (sorted[0].value == sorted[3].value) return TieBreaker(t1, sorted[4].valueint);
        else if (sorted[1].value == sorted[4].value) return TieBreaker(t1, sorted[0].valueint);

        else return 0;
    }

    public static float Three(List<Card> sorted)
    {
        int t1 = sorted[2].valueint;

        if (sorted[0].value == sorted[2].value) return TieBreaker(t1, sorted[4].valueint, sorted[3].valueint);
        else if (sorted[1].value == sorted[3].value) return TieBreaker(t1, sorted[4].valueint, sorted[0].valueint);
        else if (sorted[2].value == sorted[4].value) return TieBreaker(t1, sorted[1].valueint, sorted[0].valueint);
        else return 0;
    }

    public static float Pair(List<Card> sorted)
    {

        if (sorted[0].value == sorted[1].value) return TieBreaker(sorted[0].valueint, sorted[4].valueint, sorted[3].valueint, sorted[2].valueint);
        else if (sorted[1].value == sorted[2].value) return TieBreaker(sorted[1].valueint, sorted[4].valueint, sorted[3].valueint, sorted[0].valueint);
        else if (sorted[2].value == sorted[3].value) return TieBreaker(sorted[2].valueint, sorted[4].valueint, sorted[1].valueint, sorted[0].valueint);
        else if (sorted[3].value == sorted[4].value) return TieBreaker(sorted[3].valueint, sorted[2].valueint, sorted[1].valueint, sorted[0].valueint);
        else return 0;
    }

    public static float FullHouse(List<Card> sorted)
    {
        int t1 = sorted[2].valueint;
        if (sorted[0].value == sorted[2].value && sorted[3].value == sorted[4].value) return TieBreaker(t1, sorted[4].valueint);
        else if (sorted[0].value == sorted[1].value && sorted[2].value == sorted[4].value) return TieBreaker(t1, sorted[0].valueint);
        else return 0;
    }

    public static float TwoPair(List<Card> sorted)
    {
        int t1 = sorted[3].valueint;
        int t2 = sorted[1].valueint;
        if (sorted[0].value == sorted[1].value && sorted[2].value == sorted[3].value) return TieBreaker(t1, t2, sorted[4].valueint);
        else if (sorted[0].value == sorted[1].value && sorted[3].value == sorted[4].value) return TieBreaker(t1, t2, sorted[2].valueint);
        else if (sorted[1].value == sorted[2].value && sorted[3].value == sorted[4].value) return TieBreaker(t1, t2, sorted[0].valueint);
        else return 0;
    }

    private static float TieBreaker(int t1, int t2 = 0, int t3 = 0, int t4 = 0, int t5 = 0)
    {
        var sum = 0f;
        sum += t1 / 100f;
        sum += t2 / 10000f;
        sum += t3 / 1000000f;
        sum += t4 / 100000000f;
        sum += t5 / 10000000000f;
        return sum;
    }
}
