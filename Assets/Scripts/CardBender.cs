﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//[ExecuteAlways]
public class CardBender : MonoBehaviour
{
    [SerializeField] RectTransform viewRT;
    [SerializeField] BendingConfig settings;
    [SerializeField] Image image;

    private bool blurred;

    private void Awake()
    {
    }

    void Update()
    {
        if (viewRT == null || settings == null) return;

        var center = SpinController.instance.board.position;
        var size = SpinController.instance.size.position - SpinController.instance.board.position;
        var threshold = SpinController.instance.threshold.position.y - SpinController.instance.board.position.y;

        if (center.y - transform.position.y > threshold) Tile(Mathf.RoundToInt(0.5f * (center.y - transform.position.y) / threshold));
        if (transform.position.y - center.y > threshold) Tile(-Mathf.RoundToInt(0.5f * (transform.position.y - center.y) / threshold));

        var bias = Vector2.Scale(transform.position - center, new Vector2(1f / size.x, 1f / size.y));
        var xArg = Mathf.Clamp01(0.5f * (bias.x + 1f));
        var yArg = Mathf.Clamp01(0.5f * (bias.y + 1f));

        viewRT.localScale = Vector3.one * settings.scaleFromY.Evaluate(yArg);
        if (blurred) viewRT.localScale = Vector3.Scale(viewRT.localScale, new Vector3(1f, settings.blurredScaleFactor, 1f));
        var pitch = settings.pitchMultiplier * settings.pitchFromY.Evaluate(yArg);
        var xFactor = settings.factorFromX.Evaluate(xArg);
        var roll = xFactor * settings.rollMultiplier * settings.rollFromY.Evaluate(yArg);
        viewRT.eulerAngles = new Vector3(pitch, roll);
        var offset = xFactor * settings.offsetMultiplier * settings.offsetFromY.Evaluate(yArg);
        viewRT.anchoredPosition = Vector2.right * offset;

        //if (Input.GetMouseButtonDown(1)) Debug.Log((Vector2)transform.position + " : " + bias);
    }

    private void Tile(int dir)
    {
        var tile = SpinController.instance.tile.position.y - SpinController.instance.board.position.y;
        transform.position += dir * Vector3.up * tile;
        var reel = Mathf.RoundToInt(2f + 2f * (transform.position.x - SpinController.instance.board.position.x) / (SpinController.instance.size.position.x - SpinController.instance.board.position.x));

        if (dir > 0) image.sprite = SpinController.instance.SpriteForReel(reel, ref blurred);
    }
}
