﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DebugView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI firstSetLabel;
    [SerializeField] private TextMeshProUGUI secondSetLabel;
    [SerializeField] private TextMeshProUGUI resultLabel;
    [SerializeField] private Toggle orderToggle;
    [SerializeField] private GameConfig gameConfig;

    public static DebugView instance;

    private string sorted = "";
    private string unsorted = "";

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Game.OrderList = gameConfig.orderCards;
        orderToggle.isOn = Game.OrderList;
        Sort(Game.OrderList);
    }

    public void Show(string fs, string ss, string unsorted, string sorted, bool order)
    {
        firstSetLabel.text = fs;
        secondSetLabel.text = ss;
        this.sorted = sorted;
        this.unsorted = unsorted;
        Sort(order);
    }

    public void Sort(bool order)
    {
        var hand = order ? sorted : unsorted;
        resultLabel.text = hand;
        Game.OrderList = order;
        //if (hand != "") SpinController.instance.ResetSlot(Tools.ParceList(hand + ", " + hand + ", " + hand));
        if (hand != "") SpinController.instance.SetCenterRow(Tools.ParceList(hand));
    }

}
