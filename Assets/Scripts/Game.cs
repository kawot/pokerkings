﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;
using Random = UnityEngine.Random;
using System.Net.Sockets;
using System.Net;
using System.Text;

public class Game : MonoBehaviour
{
    public GameConfig config;
    public Dictionary<int, Sprite> normal = new Dictionary<int, Sprite>();
    public Dictionary<int, Sprite> blured = new Dictionary<int, Sprite>();
    public int[] scores = new int[] { 50, 100, 200, 400, 800, 1600, 3200, 6400, 12800, 25600 };

    public static bool OrderList;

    private List<Card> secondRow;
    private List<Card> firstRow;
    private TextMeshProUGUI CurrentWin;
    private Text CombaName;
    private Text totalScore;
    private Dealer dealer;
    private string comba;
    private string sorted;
    private string combaname;
    private int score;

    void Start()
    {
#if UNITY_ANDROID
        Application.logMessageReceived += LogUDP;
#endif
        CurrentWin = GameObject.Find("CurrentWin").GetComponent<TextMeshProUGUI>();
        
        CombaName = GameObject.Find("CombaName").GetComponent<Text>();
        totalScore = GameObject.Find("TotalScore").GetComponent<Text>();
        CombaName.text = "";
        
        dealer = GetComponent<Dealer>();
        OrderList = config.orderCards;

        for (int i = 0; i< 52;i++)
        {
            string s = i.ToString(); if (i < 10) s = '0' + s;
            
            string numname = s;
                    

            int numba = i;
                Sprite t = Resources.Load<Sprite>("Cards/" + numname);
            
                normal.Add(i, t);
            Sprite tb = Resources.Load<Sprite>("Cards/blured/" + numname);

            blured.Add(numba, tb);
        }
        print("-------------------Loaded normal Cards Textures = " + normal.Count);
        print("----------------------Loaded blured Cards Textures = " + blured.Count);
        print("---------------------- Loaded Sounds=" + SoundBank.sounds.Count);
        ResetScore();
    }
#if UNITY_ANDROID
    private void LogUDP(string logstring, string stackTrace, LogType type)
    {
        if (type == LogType.Exception || type == LogType.Error)
            logstring += "*\n************ERROR*********\n\n" + stackTrace;

        Socket socketudp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        IPEndPoint remotePoint = new IPEndPoint(IPAddress.Parse("192.168.1.120"), 9000);
        socketudp.SendTo(Encoding.ASCII.GetBytes(logstring), remotePoint);
    }
#endif
    public void ResetScore()
    {
        score = 0;
        UpdateScore(true);
    }

    private void UpdateScore(bool reset)
    {
        if (reset) dealer.coinIndex = -1;
        if (dealer.coinIndex == -1)
        {
            CombaName.text = "";
            CurrentWin.text = "";
            totalScore.text = ZapIt(score, 9);
        }
        else
        {
            CombaName.text = combaname;
            score += scores[dealer.coinIndex];
            CurrentWin.text = "$" + ZapIt(scores[dealer.coinIndex], 5);
            totalScore.text = ZapIt(score, 9);
        }
    }

    public string ZapIt(int score, int v)
    {
        if (score == 0) return "0";
        string s = score.ToString();
        string res = "";
        long d; int n = 0;
        long scor = score;
        while (res.Length <= s.Length)
        {
            d = scor % 10;

            scor = (int)scor / 10;
            res = d + res; n++;
            if (n % 3 == 0 && res.Length < v)
                res = "," + res;
        }
        if (res.Length > 0)
            if (res[0] == ',')
                res = res.Substring(1);
        return res;
    }

    public Sprite gimmeNormal(int centernum)
    {
      
        return normal[centernum];
    }
    public Sprite gimmeBlured(int centernum)
    {
        return blured[centernum];
    }

    public void SaveCombination(string comba, string sorted, string combaname)
    {
        this.combaname = combaname;
        this.comba = comba;
        this.sorted = sorted;
    }

    public void Show()
    {
        secondRow = dealer.secondRow;
        firstRow = dealer.firstRow;

        DebugView.instance.Show(String.Join(", ", firstRow.ToArray()), String.Join(", ", secondRow.ToArray()), comba, sorted, OrderList);
        
        UpdateScore(false);
        dealer.isSpinning = false;
    }

    public static List<Card> Sorted(List<Card> list)
    {
        var sorted = new List<Card>(list);
        sorted.Sort(delegate (Card c1, Card c2)
        {
            if (c1.valueint >= c2.valueint) return 1;
            else if (c1.valueint < c2.valueint) return -1;
            else return 0;
        });
        return sorted;
    }

    public Sprite CardSprite (Card card, bool isBloored = false)
    {
        var name = card.ToString();
        var index = Array.IndexOf(dealer.cards, name);
        var sprite = isBloored ? gimmeBlured(index) : gimmeNormal(index);
        return sprite;
    }

    public Sprite RandomCardSprite()
    {
        return gimmeBlured(Random.Range(0, blured.Count));
    }
}
